package uz.fido.shina.test;

import uz.fido.shina.util.Util;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {

        String schemaFileName="schemaFile";
        String requestData="data";

        String schemaStr="{\n" +
                "    \"$schema\": \"http://json-schema.org/draft-04/schema#\",\n" +
                "    \"title\": \"Product\",\n" +
                "    \"description\": \"A product from Acme's catalog\",\n" +
                "    \"type\": \"object\",\n" +
                "    \"properties\": {\n" +
                "        \"id\": {\n" +
                "            \"description\": \"The unique identifier for a product\",\n" +
                "            \"type\": \"integer\"\n" +
                "        },\n" +
                "        \"name\": {\n" +
                "            \"description\": \"Name of the product\",\n" +
                "            \"type\": \"string\"\n" +
                "        },\n" +
                "        \"price\": {\n" +
                "            \"type\": \"number\",\n" +
                "            \"minimum\": 0,\n" +
                "            \"exclusiveMinimum\": true\n" +
                "        },\n" +
                "        \"tags\": {\n" +
                "            \"type\": \"array\",\n" +
                "            \"items\": {\n" +
                "                \"type\": \"string\"\n" +
                "            },\n" +
                "            \"minItems\": 1,\n" +
                "            \"uniqueItems\": true\n" +
                "        }\n" +
                "    },\n" +
                "    \"required\": [\"id\", \"name\", \"price\"]\n" +
                "}";


//        Util.writeSchemaToFile(schemaStr,schemaFileName);
//        Validator validator = new Validator();
//        try{
//            validator.validateJson(requestData,schemaFileName);
//        }catch (Exception e){
//            System.out.printf("ERROR::: "+e.getMessage());
//        }
        System.out.printf("OK!");

    }
}
