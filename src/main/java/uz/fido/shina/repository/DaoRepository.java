package uz.fido.shina.repository;

import com.zaxxer.hikari.HikariDataSource;
import org.everit.json.schema.Schema;
import org.everit.json.schema.loader.SchemaLoader;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import uz.fido.shina.exception.CustomizedRequestException;
import uz.fido.shina.model.JsonSchema;
import uz.fido.shina.model.RequestDetail;
import uz.fido.shina.model.ResponseDetail;
import uz.fido.shina.model.Result;


import java.sql.*;

@Repository
public class DaoRepository {
    private final HikariDataSource ds;

    @Autowired
    public DaoRepository(HikariDataSource ds) {
        this.ds = ds;
    }

    private PreparedStatement ps = null;
    private ResultSet rs = null;
    private CallableStatement cs = null;
    private Connection conn = null;
    private static final Logger logger = LoggerFactory.getLogger(DaoRepository.class);


    public String checkMethod(String methodName, String username, String password, String jsonRequest) throws SQLException {

        String response = null;
        String schema = null;
        String error_code = null;
        String error_msg = null;
        try {
            conn = ds.getConnection();
            cs = conn.prepareCall("{call Esb_Api.Check_access(?, ?, ?, ?, ?, ?)}");
            cs.setString(1, methodName);
            cs.setString(2, username);
            cs.setString(3, password);
            cs.registerOutParameter(4, Types.VARCHAR);
            cs.registerOutParameter(5, Types.VARCHAR);
            cs.registerOutParameter(6, Types.VARCHAR);
            cs.execute();
            error_code = cs.getString(4);
            error_msg = cs.getString(5);
            schema = cs.getString(6);

        } finally {
            if (cs != null) {
                cs.close();
            }
            if (conn != null) {
                conn.close();
            }
        }

        boolean responseOfValidate =true;
//        validateRequest(jsonRequest, schema, error_code, error_msg);

        if (responseOfValidate) {
            response = receiver(jsonRequest);
        }

        return response;
    }

    /*--------------------------------------receiver----------------------------------------------------*/


    public String receiver(String request) throws SQLException {

        String repsonse = null;
        try {
            conn = ds.getConnection();
            cs = conn.prepareCall("{call Esb_Api.Recv(?, ?)}");
            cs.setString(1, request);
            cs.registerOutParameter(2, Types.VARCHAR);
            cs.execute();
            repsonse = cs.getString(2);
        } finally {
            if (cs != null) {
                cs.close();
            }
            if (conn != null) {
                conn.close();
            }
        }

        return repsonse;
    }





    /*---------------------------------check_access---------------------------------------------------------*/


    public JsonSchema checkAccess(RequestDetail requestDetail) throws SQLException {

        JsonSchema jsonSchema = new JsonSchema();

        try {
            conn = ds.getConnection();
            cs = conn.prepareCall("{call Esb_Api.Has_Access(?, ?, ?, ?, ?, ?, ?, ?, ?)}");

            cs.setString(1, requestDetail.getUsername());
            cs.setString(2, requestDetail.getPassword());
            cs.setString(3, requestDetail.getServiceName());
            cs.setString(4, requestDetail.getMethodName());
//            cs.setString(5, requestDetail.getRequestId());

            cs.registerOutParameter(5, Types.VARCHAR);
            cs.registerOutParameter(6, Types.VARCHAR);
            cs.registerOutParameter(7, Types.NUMERIC);
            cs.registerOutParameter(8, Types.CLOB);
            cs.registerOutParameter(9, Types.CLOB);

            cs.execute();

            jsonSchema.setCode(cs.getString(5));
            jsonSchema.setMsg(cs.getString(6));
            jsonSchema.setMethod_uid(cs.getInt(7));

            Clob in_json_schema_clob = cs.getClob(8);
            Clob out_json_schema_clob = cs.getClob(9);


            jsonSchema.setIn_json_schema(in_json_schema_clob.getSubString(1, (int) in_json_schema_clob.length()));
            jsonSchema.setOut_json_schema(out_json_schema_clob.getSubString(1, (int) out_json_schema_clob.length()));

        } finally {
            if (cs != null) {
                cs.close();
            }
            if (conn != null) {
                conn.close();
            }
        }

        return jsonSchema;
    }

    /*---------------------------------Perform request---------------------------------------------------------*/
    public ResponseDetail performRequest(RequestDetail requestDetail, Integer method_uid) throws SQLException {

        ResponseDetail responseDetail = new ResponseDetail();

        String repsonse = null;
        try {
            conn = ds.getConnection();
            cs = conn.prepareCall("{call Esb_Api.Recv(?, ?, ?, ?, ?, ?)}");
            cs.setInt(1, method_uid);
            cs.setString(2, requestDetail.getRequestId());
            cs.setString(3, requestDetail.getRequestBody());
            cs.registerOutParameter(4, Types.VARCHAR);
            cs.registerOutParameter(5, Types.VARCHAR);
            cs.registerOutParameter(6, Types.VARCHAR);
            cs.execute();
            responseDetail.setRequest_id(requestDetail.getRequestId());
            responseDetail.setCode(cs.getString(4));
            responseDetail.setMsg(cs.getString(5));
            responseDetail.setResponseBody(cs.getString(6));
        } finally {
            if (cs != null) {
                cs.close();
            }
            if (conn != null) {
                conn.close();
            }
        }

        return responseDetail;
    }

    /*---------------------------------Cancel request---------------------------------------------------------*/
    public Result cancelRequest(Integer method_uid, String requestId, String reasonForCancellation) throws SQLException {

        Result result = new Result();

        String repsonse = null;
        try {
            conn = ds.getConnection();
            cs = conn.prepareCall("{call Esb_Api.Recv(?, ?, ?, ?, ?)}");
            cs.setInt(1, method_uid);
            cs.setString(2, requestId);
            cs.setString(3, reasonForCancellation);
            cs.registerOutParameter(4, Types.NUMERIC);
            cs.registerOutParameter(5, Types.VARCHAR);
            cs.execute();

            result.setCode(cs.getInt(4));
            result.setDescription(cs.getString(5));

        } finally {
            if (cs != null) {
                cs.close();
            }
            if (conn != null) {
                conn.close();
            }
        }

        return result;
    }

}
