package uz.fido.shina.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.ResourceUtils;

import javax.annotation.Resource;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;


public class Util {

    private static final Logger logger = LoggerFactory.getLogger(Util.class);


    /**
     * SQL uchun ' simbolini to'g'irlash O'zbek -> O''zbek
     * <pre>
     * String name = request.getParameter("name");
     * stored.execSelect("select * from my_table where name = " + Util.quotesSQL(name));
     * </pre>
     */
    public static String quotesSQL(String instr) {
        // функция удваиваетвстречающиеся символы апострофа , т.е. заменяет ' на '' - используется в SQL стрингах
        if (instr == null) {
            return null;
        }
        int lastPos = 0;
        int k = -1;
        while ((k = instr.indexOf(39, lastPos)) != -1) {
            instr = instr.substring(0, k) + "'" + instr.substring(k);
            lastPos = k + 2;
        }
        return instr;
    }

    public static String stackTrace(Throwable ex) {
        StackTraceElement[] ste = ex.getStackTrace();
        StringBuilder sb = new StringBuilder();
        sb.append(ex.getMessage());
        for (int i = 0; i < ste.length; i++) {
            sb.append(ste[i].toString());
            sb.append("\n");
            if (i == 10) {
                break;
            }
        }
        return sb.toString();
    }

    public static void writeSchemaToFile(String schema,String filename) throws IOException {


        Files.write(Paths.get("f:\\SHINA\\shina\\src\\main\\resources\\jsonSchema\\"+filename),schema.getBytes());

        File fileSchema = ResourceUtils.getFile("classpath:jsonSchema/"+filename);
        logger.info("File Found :  " + fileSchema.exists());

        if(fileSchema.exists()){
            System.out.println("CREATED!");
        }else {
            System.out.println("NOT CREATED!");
        }


    }


}
