package uz.fido.shina.exception;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import uz.fido.shina.model.Result;
import uz.fido.shina.util.Util;




@ControllerAdvice
@RestController
public class CustomizedResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    private static final Logger logger = LoggerFactory.getLogger(CustomizedResponseEntityExceptionHandler.class);

    @ExceptionHandler(Exception.class)
    public final ResponseEntity<Result> handleAllExceptions(Exception ex, WebRequest request) {
        logger.error("" + Util.stackTrace(ex));
        Result result = new Result();
        result.setCode(5);
        result.setDescription("" + ex + " Request:" + request.getDescription(false));
        return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }



    @ExceptionHandler(CustomizedRequestException.class)
    public final ResponseEntity<?> handleIlegalPageNumberException(CustomizedRequestException ex, WebRequest request) {
        logger.error("" + Util.stackTrace(ex));
        Result result = new Result(ex.getCode(), ex.getMessage());
        return new ResponseEntity<>(result, HttpStatus.valueOf(ex.getHttpResponseCode()));
    }


}
