package uz.fido.shina;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ShinaApplication {

	public static void main(String[] args) {
		SpringApplication.run(ShinaApplication.class, args);
	}

}
