package uz.fido.shina.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.everit.json.schema.Schema;
import org.everit.json.schema.loader.SchemaLoader;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.fido.shina.exception.CustomizedRequestException;
import uz.fido.shina.model.JsonSchema;
import uz.fido.shina.model.RequestDetail;
import uz.fido.shina.model.ResponseDetail;
import uz.fido.shina.model.Result;
import uz.fido.shina.repository.DaoRepository;

import java.sql.SQLException;

@Service
public class ShinaService {

    @Autowired
    private DaoRepository daoRepository;

    private static final Logger logger = LoggerFactory.getLogger(ShinaService.class);


    public boolean cryto(String requestCrypto, RequestDetail requestDetail) {
        /*I will check here requestCrypto with encrypted requestDetail */

        return true;
    }


    public ResponseDetail prepareResponse(RequestDetail requestDetail) throws SQLException, JsonProcessingException {


        ObjectMapper mapper = new ObjectMapper();
        ResponseDetail responseDetail = null;
        boolean isResponseDetailValid = false;

        /*sending request details to DB*/
        JsonSchema jsonSchema = daoRepository.checkAccess(requestDetail);

        /*checking code after has_access (after sending user credentials, method name, service name to DB)*/
        if (jsonSchema.getCode().equals("00000")) {

            //converting Java object(requestDetail) to JSON string - pretty-print
            String requestDetailJsonInString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(requestDetail);

            //checking request according to in_json_schema
            boolean isRequestDetailValid = validate_jsonRequest(requestDetailJsonInString, jsonSchema.getIn_json_schema());

            /*if request body is valid, we will send it to DB with method uid*/
            if (isRequestDetailValid) {
                responseDetail = daoRepository.performRequest(requestDetail, jsonSchema.getMethod_uid());
            }

        } else {
            throw new CustomizedRequestException(jsonSchema.getMsg(), Integer.parseInt(jsonSchema.getCode()), 400);
        }
        /*checking response code after perform (after sending request to DB)*/
        if (responseDetail.getCode().equals("00000")) {

            //converting Java objects (responseDetail) to JSON string - pretty-print
            String responseDetailJsonInString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(responseDetail);

            //checking response according to out_json_schema
            isResponseDetailValid = validate_jsonResponse(responseDetailJsonInString, jsonSchema.getOut_json_schema(), jsonSchema.getMethod_uid(), requestDetail.getRequestId());


        } else {
            throw new CustomizedRequestException(responseDetail.getMsg(), Integer.parseInt(responseDetail.getCode()), 400);
        }

        /*if response from DB is valid, we will send (return) it to Client */

        if (isResponseDetailValid) {
            return responseDetail;
        }

        return null;
    }

    private boolean validate_jsonRequest(String JsonRequest, String jsonSchemaFromDb) {
        JSONObject jsonSchema = new JSONObject(new JSONTokener(jsonSchemaFromDb));
        JSONObject jsonSubjectData = new JSONObject(new JSONTokener(JsonRequest));

        Schema schema = SchemaLoader.load(jsonSchema);
        try {
            schema.validate(jsonSubjectData);
        } catch (Exception e) {
            logger.info("ERROR::  " + e.getMessage());
            logger.error("ERROR::  " + e.getMessage());
//            return false;
            throw new CustomizedRequestException(e.getMessage(), 2, 400);
        }
        return true;
    }

    /*-Validate Json Response-*/
    private boolean validate_jsonResponse(String JsonResponse, String jsonSchemaFromDb, Integer method_uid, String requestId) throws SQLException {

        JSONObject jsonSchema = new JSONObject(new JSONTokener(jsonSchemaFromDb));
        JSONObject jsonSubjectData = new JSONObject(new JSONTokener(JsonResponse));

        Schema schema = SchemaLoader.load(jsonSchema);
        try {
            schema.validate(jsonSubjectData);
        } catch (Exception e) {
            logger.info("ERROR::  " + e.getMessage());
            logger.error("ERROR::  " + e.getMessage());
            Result result = daoRepository.cancelRequest(method_uid, requestId, e.getMessage());

            logger.info("Request Cancellation result::  code: " + result.getCode() + "\n Description: " + result.getDescription());

            throw new CustomizedRequestException(result.getDescription(), result.getCode(), 500);

        }
        return true;
    }


}
