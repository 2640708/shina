package uz.fido.shina.model;

public class Request {
    private RequestDetail requestDetail;
    private String RequestCryto;

    public RequestDetail getRequestDetail() {
        return requestDetail;
    }

    public void setRequestDetail(RequestDetail requestDetail) {
        this.requestDetail = requestDetail;
    }

    public String getRequestCryto() {
        return RequestCryto;
    }

    public void setRequestCryto(String requestCryto) {
        RequestCryto = requestCryto;
    }
}
