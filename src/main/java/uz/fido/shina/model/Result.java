package uz.fido.shina.model;


public class Result {

    private Integer code;
    private String description;

    public Result(Integer code, String description) {
        super();
        this.code = code;
        this.description = description;
    }

    public Result() {
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDescription() {
                return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
