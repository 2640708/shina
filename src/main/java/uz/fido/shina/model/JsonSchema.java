package uz.fido.shina.model;

public class JsonSchema {
    private String code;
    private String msg;
    private Integer method_uid;
    private String in_json_schema;
    private String out_json_schema;

    public Integer getMethod_uid() {
        return method_uid;
    }

    public void setMethod_uid(Integer method_uid) {
        this.method_uid = method_uid;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getIn_json_schema() {
        return in_json_schema;
    }

    public void setIn_json_schema(String in_json_schema) {
        this.in_json_schema = in_json_schema;
    }

    public String getOut_json_schema() {
        return out_json_schema;
    }

    public void setOut_json_schema(String out_json_schema) {
        this.out_json_schema = out_json_schema;
    }
}
