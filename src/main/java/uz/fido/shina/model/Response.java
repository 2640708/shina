package uz.fido.shina.model;

public class Response {
    private ResponseDetail responseDetail;
    private String responseCryto;

    public ResponseDetail getResponseDetail() {
        return responseDetail;
    }

    public void setResponseDetail(ResponseDetail responseDetail) {
        this.responseDetail = responseDetail;
    }

    public String getResponseCryto() {
        return responseCryto;
    }

    public void setResponseCryto(String responseCryto) {
        this.responseCryto = responseCryto;
    }
}
