package uz.fido.shina.controller;

import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.fido.shina.model.Request;
import uz.fido.shina.model.RequestDetail;
import uz.fido.shina.model.Response;
import uz.fido.shina.repository.DaoRepository;
import uz.fido.shina.service.ShinaService;

import java.io.IOException;
import java.sql.SQLException;

@Api(tags = "Shina", description = "Shina management related methods")
@RestController
@RequestMapping(value = {"/api"})
public class ShinaController {
    private static final Logger logger = LoggerFactory.getLogger(ShinaController.class);


    @Autowired
    private DaoRepository daoRepository;
    @Autowired
    private ShinaService shinaService;


    /**
     * POST request in shina
     */
    @ApiOperation("Request in shina")
    @PostMapping(value = {"/receive"}, produces = {"application/json"})
    public ResponseEntity<?> Receiver(
            @RequestHeader(value = "method", required = false) String methodName,
            @RequestHeader(value = "username", required = false) String username,
            @RequestHeader(value = "password", required = false) String password,
            @ApiParam(name = "Request body", required = true) @RequestBody String jsonRequest) throws SQLException, IOException {

        String response = daoRepository.checkMethod(methodName, username, password, jsonRequest);

        logger.info("res: " + response);

        return ResponseEntity.status(200).body(response);


    }

    /**
     * POST request in shina
     */
    @ApiOperation("Request for shina2")
    @PostMapping(value = {"/send"}, produces = {"application/json"})
    public ResponseEntity<?> RequestReceiver(
            @ApiParam(name = "Request body", required = true) @RequestBody Request jsonRequest) throws SQLException, IOException {

        Response response = new Response();

//        String response = daoRepository.checkMethod(requestDetail.getMethodName(), requestDetail.getUsername(), requestDetail.getPassword(), requestDetail.getRequestBody());

        response.setResponseDetail(shinaService.prepareResponse(jsonRequest.getRequestDetail()));
        response.setResponseCryto("Crypto");

//        logger.info("res: " + response);


        return ResponseEntity.status(200).body(response);


    }
}
